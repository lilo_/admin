# coding=utf-8
from __future__ import unicode_literals

from django.db import models


# Create your models here.


class Banner(models.Model):
    class Meta:
        db_table = 'banner'
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'

    title = models.CharField(max_length=255, verbose_name='Наименование')
    short_description = models.CharField(max_length=1000, verbose_name='Краткое описание')
    description = models.TextField(verbose_name='Описание')
    image = models.ImageField(upload_to='', verbose_name='Изображение')

    def __unicode__(self):
        return self.title
